﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/**
 * Clase que controla el movimiento de la camara.
 */
public class CameraManager : MonoBehaviour {
    
    public Transform target;

    private Transform thisTr;


    private void Awake() {
        thisTr = transform;
    }


    /**
     * Avanza lateralmente cuando el player se desplaza.
     */
    void Update() {
        Vector3 pos = thisTr.position;
        pos.x = target.position.x;
        thisTr.position = pos;
    }
}
