﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

/**
 * Clase que controla el comportamiento general del juego y los elementos de la UI. 
 */
public class GameManager : MonoBehaviour {

    public GameObject[] CollectablesPrefab;
    public PlayerController Player;
    public Animation FlagAnim;
    public MusicManager Music;

    public Text PointsText, CoinsText, TimeText, GameOverText;
    public int KillEnemyPoints, CatchPowerupPoints, BreakBlockPoints, WinPoints, EachSecondPoints, Time;

    private int points, coins, time;
    private bool isGameOver;


    private void Awake() {
        time = Time;
        PointsText.text = 0 + "";
        CoinsText.text = 0 + "";
        isGameOver = false;
        InvokeRepeating("AddTimeEffect", 0f, 1f);
    }


    /**
     * Metodo utilizado por los bloques cuando
     * muestran una moneda o un powerup.
     */
    public GameObject GetRandomCollectable() {
        return CollectablesPrefab[Random.Range(0, CollectablesPrefab.Length)];
    }


    /**
     * Meotodos que suman puntos por las acciones
     * de matar enemigos, coger powerups y romper bloques.
     */
    public void KillEnemy() {
        Music.PlayKillSound();
        points += KillEnemyPoints;
        PointsText.text = points + "";
    }
    public void CatchPowerup() {
        Music.PlayPowerupSound();
        points += CatchPowerupPoints;
        PointsText.text = points + "";
    }
    public void BreakBlock() {
        Music.PlayBreakBlockSound();
        points += BreakBlockPoints;
        PointsText.text = points + "";
    }

    /**
     * Corutina que genera el efecto de sumar puntos
     * segun la cantidad de tiempo restante, en lugar
     *  de sumarlos todos de golpe.
     *  Recarga la escena para empezar de nuevo.
     */
    private IEnumerator AddPointsEffect() {
        while (time > 0) {
            TimeText.text = --time + "";
            points += EachSecondPoints;
            PointsText.text = points + "";
            yield return null;
        }
        GameOverText.text = "VOLVIENDO A EMPEZAR...";
        yield return new WaitForSeconds(3f);
        ReloadScene();
    }

    public void AddCoin() {
        Music.PlayCoinSound();
        CoinsText.text = ++coins + "";
    }

    /**
     * Metodo que se repite cada segundo para actualizar
     * el contador de tiempo y, si llega a 0, indicar
     * que ha perdido la partida.
     */
    private void AddTimeEffect() {
        if (!isGameOver) {
            if (time > 0)
                TimeText.text = --time + "";
            else
                Player.Die();
        }
    }


    
    public void FlagDown() {
        FlagAnim.Play();
        points += WinPoints;
        PointsText.text = points + "";
    }
    public void Win() {
        Music.StopSong();
        Music.PlayWinSound();
        isGameOver = true;
        GameOverText.text = "HAS GANADO!";
        StartCoroutine(AddPointsEffect());
    }
    public void Die() {
        Music.StopSong();
        Music.PlayDieSound();
        //Music.PlayLoseSound();
        isGameOver = true;
        GameOverText.text = "HAS PERDIDO :(";
        Invoke("ReloadScene", 3f);
    }
    public void ReloadScene() {
        SceneManager.LoadScene("SampleScene");
    }


    public void PlayJumpSound() {
        Music.PlayJumpSound();
    }
}
