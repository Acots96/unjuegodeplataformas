# PEC2

Este es un juego de plataformas basado en el clásico videojuego Super Mario Bros. Concretamente en el primer nivel.

Consta de una sola escena con los siguientes elementos:
 - Personaje jugable de Mario.
 - Enemigos (únicamente los que tienen forma de champiñón).
 - Powerups (únicamente la flor).
 - Monedas
 - Bloques del suelo.
 - Bloques de plataformas (ladrillo e interrogación).
 - Tuberías verdes.
 - Poste con bandera.
 - Castillo al final del nivel.
 - Elementos decorativos (montañas, arbustos y nubes).
 
También tiene los 4 elementos de la UI:
 - Puntuación del jugador.
 - Monedas obtenidas.
 - Indicador del nivel.
 - Tiempo restante.

Enlace al video de muestra: https://youtu.be/yFkuyzgLVTE


# Estructuración e implementación

El nivel está estructurado tratando de imitar la estética del nivel original, por lo que todos los elementos del terreno están en la misma posición, sino parecida.

El juego consta de diversas clases:
 - GameManager: Controlador principal del juego que se encarga básicamente de controlar y actualizar los elementos de la UI, de controlar el MusicManager para los sonidos y la canción de fondo, de proporcionar el Colectable (powerup o moneda) al CubeController y finalmente de llevar a cabo las acciones pertinentes cuando el jugador gana o pierde.
 - MusicManager: Controlador que provee a GameManager de las funciones necesarias para ejecutar los sonidos correspondientes en cada momento.
 - PlayerController: Controlador del jugador que mueve al personaje de Mario según el input recibido del jugador. También se encarga de comprobar sus colisiones con los demás elementos como los enemigos, los bloques (suelo y plataformas) y los Colliders pertinentes de ganar y morir. Cada elemento tiene un layer/tag concreto que utiliza el script para diferenciar unos elementos de otros y tomar la decisión correspondiente. Finalmente gestiona los powerups y comunica al GameController cada acción realizada dado que la mayoría de las acciones conllevan cambios en la UI (más puntuación).
 - EnemyController: Controlador de los enemigos que mueve cada personaje horizontalmente utilizando un collider con un layer concreto para hacerlo cambiar de dirección. El enemigo tiene un comportamiento distinto dependiendo de la parte que colisione con Mario: Si colisiona con el enemigo por encima, este muere, mientras que si colisiona lateralmente Mario muere. Para ello he creado 3 colliders distintos para gestionar la interacción de Mario con cada enemigo.
 - CubeController: Controlador de los bloques de las plataformas, que gestiona la colisión de Mario con la parte inferior de cada bloque: utiliza un rigidbody para hacer "saltar" al bloque e instancia el Colectable (aleatorio) obtenido de GameController.
 - CameraController: Controlador de la cámara, cuyo único objetivo es el de seguir a Mario horizontalmente.

Para conocer más en detalle la implementación echa un vistazo a las cabeceras de las clases y sus métodos.


